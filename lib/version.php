<?php

$CMS_VERSION = "2.2.10";
$CMS_VERSION_NAME = "Edited by Lee Peace [ 2019 ]";
$CMS_SCHEMA_VERSION = '202';

define('CMS_VERSION', $CMS_VERSION);
define('CMS_VERSION_NAME', $CMS_VERSION_NAME);
define('CMS_SCHEMA_VERSION', $CMS_SCHEMA_VERSION);

<?php

if (!isset($gCms))
    exit;

#The tabs
echo $this->StartTabHeaders();

if ($this->CheckPermission('Modify Products')) {
    echo $this->SetTabHeader('products', $this->Lang('products'), '', 'system');
    if($this->GetPreference('allowmaps')){
        echo $this->SetTabHeader('locations', $this->Lang('locations'));
    }
    echo $this->SetTabHeader('fielddefs', $this->Lang('fielddefs'), '', 'system');
    echo $this->SetTabHeader('hierarchy', $this->Lang('product_hierarchy'), '', 'system');
    echo $this->SetTabHeader('categories', $this->Lang('categories'), '', 'system');
}

if ($this->CheckPermission('Modify Site Preferences')) {
    echo $this->SetTabHeader('prefs', $this->Lang('preferences'), '', 'system');
}
echo $this->EndTabHeaders();

#The content of the tabs
echo $this->StartTabContent();
if ($this->CheckPermission('Modify Products')) {
    echo $this->StartTab('products', $params);
    include(__DIR__ . '/function.admin_productstab.php');
    echo $this->EndTab();
    
    echo $this->StartTab('locations', $params);
    include(__DIR__ . '/function.admin_locations_tab.php');
    echo $this->EndTab();

    echo $this->StartTab('fielddefs', $params);
    include(__DIR__ . '/function.admin_fielddefs_tab.php');
    echo $this->EndTab();

    echo $this->StartTab('hierarchy', $params);
    include(__DIR__ . '/function.admin_hierarchy_tab.php');
    echo $this->EndTab();

    echo $this->StartTab('categories', $params);
    include(__DIR__ . '/function.admin_categories_tab.php');
    echo $this->EndTab();
}

if ($this->CheckPermission('Modify Site Preferences')) {
    echo $this->StartTab('prefs', $params);
    include(__DIR__ . '/function.admin_prefstab.php');
    echo $this->EndTab();
}

echo $this->EndTabContent();

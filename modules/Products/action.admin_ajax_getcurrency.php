<?php
if (!isset($gCms)) exit;
if (!$this->CheckPermission('Modify Products')) return;
$name = "Exchange Custom";
$priceExchange = $db->GetRow('SELECT value FROM ' . cms_db_prefix() . 'module_customgs WHERE name=?', array($name));
$priceExchange = ($priceExchange) ? $priceExchange["value"] : 1;

$discount = "Discount";
$priceDiscount = $db->GetRow('SELECT value FROM ' . cms_db_prefix() . 'module_customgs WHERE name=?', array($discount));
$priceDiscount = ($priceDiscount) ? $priceDiscount["value"] : 1;

$price = \cge_param::get_int($params,'price');

if ($priceExchange != 1) {
    $price = $price * $priceExchange * $priceDiscount/100;
    $price = "= ".number_format($price,0, '.', ',')." VND";
}else{
    $price = $priceExchange;
    $price = "= ".number_format($price,0, '.', ',')." VND";
}
// give it back via ajax
\cge_utils::send_ajax_and_exit($price);

#
# EOF
#
?>
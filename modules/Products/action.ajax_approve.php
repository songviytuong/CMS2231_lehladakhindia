<?php

if (!defined('CMS_VERSION'))
    exit;
//if (!$this->CheckPermission($this->_GetModuleAlias() . '_modify_item')) return;

if (!isset($params['approve']) || !isset($params['lcid'])) {
    die('missing parameter, this should not happen');
}

$id = (int) $params['lcid'];
$active = (bool) $params['approve'];

$query = 'UPDATE ' . cms_db_prefix() . 'module_products_locations SET active = ? WHERE id = ?';
$db->Execute($query, array($active, $id));

$admintheme = cms_utils::get_theme_object();
$json = new stdClass;

if ($active) {
    $json->image = $admintheme->DisplayImage('icons/system/true.gif', $this->ModLang('revert'), '', '', 'systemicon');
    $json->href = $this->CreateLink($id, 'ajax_approve', $returnid, '', array('approve' => 0, 'lcid' => $id), '', true);
} else {

    $json->image = $admintheme->DisplayImage('icons/system/false.gif', $this->ModLang('approve'), '', '', 'systemicon');
    $json->href = $this->CreateLink($id, 'ajax_approve', $returnid, '', array('approve' => 1, 'lcid' => $id), '', true);
}

// Fix URL for JSON output
$json->href = html_entity_decode($json->href);
header("Content-type:application/json; charset=utf-8");
$handlers = ob_list_handlers();
for ($cnt = 0; $cnt < sizeof($handlers); $cnt++) {
    ob_end_clean();
}
echo json_encode($json);
exit();
?>
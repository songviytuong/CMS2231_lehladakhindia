<?php

if (!isset($gCms))
    exit;
if (!$this->CheckPermission('Modify Products') || !$this->CheckPermission('Products Locations'))
    exit;

$entryarray = array();

$query = "SELECT * FROM " . cms_db_prefix() . "module_products_locations ORDER BY id";
$dbresult = $db->Execute($query);

$rowclass = 'row1';
$theme = cms_utils::get_theme_object();
while ($dbresult && $row = $dbresult->FetchRow()) {
    $onerow = new stdClass();

    $onerow->id = $row['id'];
    $onerow->title = $row['title'];
    $onerow->latitude = $row['latitude'];
    $onerow->longitude = $row['longitude'];
    
    if ($row['active']) {
        $activeimage = $theme->DisplayImage('icons/system/true.gif', $this->Lang('setfalse'), '', '', 'systemicon');
        $approve = 0;
    } else {
        $activeimage = $theme->DisplayImage('icons/system/false.gif', $this->Lang('settrue'), '', '', 'systemicon');
        $approve = 1;
    }

    $onerow->edit_url = $this->create_url($id, 'editlocation', $returnid, array('lcid' => $row['id']));
    $onerow->activelink = $this->CreateLink($id, 'ajax_approve', $returnid, $activeimage, array('lcid' => $row['id'], 'approve' => $approve));
    $onerow->editlink = $this->CreateLink($id, 'editlocation', $returnid, $theme->DisplayImage('icons/system/edit.gif', $this->Lang('edit'), '', '', 'systemicon'), array('lcid' => $row['id']));
    $onerow->deletelink = $this->CreateLink($id, 'deletelocation', $returnid, $theme->DisplayImage('icons/system/delete.gif', $this->Lang('delete'), '', '', 'systemicon'), array('lcid' => $row['id']), $this->Lang('areyousure'));

    $entryarray[] = $onerow;
}
//
$this->smarty->assign_by_ref('items', $entryarray);
$this->smarty->assign('itemcount', count($entryarray));

$this->smarty->assign('addlink', $this->CreateLink($id, 'addlocation', $returnid, $theme->DisplayImage('icons/system/newfolder.gif', $this->Lang('addfielddef'), '', '', 'systemicon'), array(), '', false, false, '') . ' ' . $this->CreateLink($id, 'addfielddef', $returnid, $this->Lang('addlocation'), array(), '', false, false, 'class="pageoptions"'));
#Display template
echo $this->ProcessTemplate('locationlist.tpl');

#
# EOF
#

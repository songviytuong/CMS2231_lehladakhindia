<ul>
	<li>v1.0 - Initial stable release.</li>
	<li>v1.1 - September 2017
		<p>- Minor bug fix.</p>
		<p>- Adds basic import and export functionality.</p>
	</li>

	<li>v1.1.2 - September 2017
		<p>- Improves the edit-form interface in the event of severe errors when parsing the form HTML.</p>
	        <p>- Improves messages about unknown HTML5 elements.  Will look at using a different library in later versions.</p>
		<p>- Minor bug fix related to static use of non-static method.</p>
	</li>
	<li>v1.2 - November 2017
		<p><strong>Note:</strong> For all forms using captcha, you may need to remove and re-add any &quot;Validate Captcha&quot; validation in the validation tab to ensure that captcha is validated on the server side.</p>
		<ul>
		<li>Fixes to captcha validation (wasn't happening server side)</li>
		<li>Can now auto-add valiations even if there are existing validations.</li>
		<li>Minor fix to template help stuff.</li>
		<li>Adds a new ModuleReference class and use it in at least one place to allow storing a simple reference to a module to use invisibly.</li>
		</ul>
	</li>
	<li>v1.2.1 - December 2017
		<ul>
		<li>FormResponse now contains a translator member and a get_translator() method.  May remove these later.</li>
		<li>Improvements to validation errors.</li>
		<li>Now handle disposition errors similar to validation errors.</li>
		<li>Smarty templates can now access raw response data.</li>
		<li>more...</li>
		</ul>
	</li>
	<li>v1.3 - December 2017
		<ul>
	        <li>Remove the reset button from the final message template.</li>
		<li>Fix issue with the honeypot field not being provided to the response object properly.</li>
	        <li>Fix issue with submissions when there are multiple forms on a page.</lli>
	        <li>Fix labels in the Smarty Template validation.</li>
	        <li>Fix issue with validations throwing the wrong exception.</li>
	        <li>Fix the select in the Dropdown-Email validation (you may need to remove and re-add this validation on your forms).</li>
		<li>fix problem that resulted in an empty template being imported.</li>
	        <li>Unique filename disposition now has a filename template.</li>
		</ul>
	</li>
	<li>v1.4.x
		<ul>
	        <li>Adds the ability to dispose via email to all members of an admin group.</li>
	        <li>data-cgbf-label now works for select fields.</li>
	        <li>Improvements to extraction of labels.</li>
		<li>Minor fix to the StopDisposing Disposition.</li>
		<li>Now use filter_var on all response fields.</li>
		</ul>
	</li>
	<li>v1.5.x
		<ul>
		<li>Fixes for entities in validation and handler templates.</li>
		<li>Now ensure that multiselect field names end with [] when rendering.</li>
		<li>Help improvements.</li>
		<li>Fix for case when the form is inline and empbedded within a detail view or some other view of another module.</li>
		</ul>
	</li>
	<li>v1.6.x
		<ul>
	        <li>Now allow specifying a smarty resource as a template.</li>
	        <li>No longer check if form needs scanning on load if using file based template.</li>
	        <li>Modify the form response object to have a has_errors() method that can be used in validation templates.</li>
	        <li>Add ability to disable a disposition.</li>
	        <li>Adds config entries for skipping version and date checks.</li>
		</ul>
	</li>
	<li>v1.7.x
		<ul>
		<li>Adds EmailAdminGroupWithReplyTo disposition.</li>
		<li>Numerous bug fixes.</li>
		</ul>
	</li>
	<li>v1.8.x
	        <ul>
		<li>Minor bug fixes.</li>
	        <li>Adds the data-cgbf-replyto attribute that is used to mark a field as an email field that contains a replyto address for admin emails.</li>
	        <li>Modifies the web hook disposition so that the 'extra variables' text area is treated as a smarty template.
	        </ul>
	</li>
	<li>v1.9.x
	<ul>
  		<li>Now auto-set the value of fields (including checkboxes, and radio buttons)
			<p>when rendering a form, if there is no specific content set in the value, we set the value, content, checked, or selected state appropriately based on the value set in the form response.</p>
	        </li>
		<li>Adds the data-cgbf-noautovalue attribute to avoid setting the value automatically.  This attribute works globally for a form, or on each individual field.</li>

		<li>Now ensure that checkbox groups have the [] appended to their name on rendering.</li>
	        <li>Adds the data-cgbf-nonameadjust attribute to avoid attempting to auto-append [] to the name of multiselect fields, or checkbox groups.</li>
	        <li>Email dispositions will now do nothing if the result of passing the body template or subject template through smarty result in empty strings.</li>
		<li>For performance reasons, only allow validation editors to be registered from admin requests.</li>
	</ul>
	</li>
</ul>

<?php
namespace CGBetterForms;

trait DOMLabelFieldTrait
{
    private $_label;

    protected function findParentLabel(\DOMElement $node)
    {
        // a function, given a label to get some text.
        $get_labeltext = function(\DomNode $node) {
            if( strtolower($node->nodeName) != 'label' ) return;
            if( !$node->hasChildNodes() ) return $child->nodeValue;
            $out = null;
            foreach( $node->childNodes as $child ) {
                if( $child->nodeType == 3 ) $out .= $child->textContent;
            }
            return $out;
        };

        // if we have a data-cgbf-label attribute
        if( ($txt = $node->getAttribute('data-cgbf-label')) ) {
            return $txt;
        }

        // if we were wrapped by a label
        if( $node->parentNode && strtolower($node->parentNode->nodeName) == 'label' ) {
            return $get_labeltext($node->parentNode);
        }

        // search backwards through peers for a label.
        $prev = ($node->previousSibling) ? $node->previousSibling  : null;
        while( $prev ) {
            $nodename = strtolower($prev->nodeName);
            if( $nodename == 'input' || $nodename == 'select' || $nodename == 'textarea' ) break;
            if( $nodename == 'label' ) {
                return $get_labeltext($prev);
            }
            $prev = $prev->previousSibling;
        }

        // check up to 3 levels of parent for a label with a for attribute that matches this id.
        $current_id = $node->getAttribute('id');
        if( $current_id ) {
            $parent = $node;
            for( $i = 0; $i < 5; $i++ ) {
                if( $parent->parentNode ) $parent = $parent->parentNode;
            }

            $walk = function($children) use (&$walk, &$get_labeltext) {
                foreach( $children as $child ) {
                    if( strtolower($child->nodeName) == 'label' && ((string) $child->getAttribute('for') == $current_id) ) {
                        return $get_labeltext($child);
                    }
                    else if( $child->hasChildNodes() ) {
                        return $walk($child->childNodes);
                    }
                }
            };
            $text = $walk($node->childNodes);
            if( $text ) return $text;
        }

    }

    public function get_label()
    {
        return $this->_label;
    }

    public function set_label($str)
    {
        $this->_label = trim($str);
    }
} // end of trait
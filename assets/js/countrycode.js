var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
///////////////////////////Country and country code Array
var ct_arr = Array(
Array('Afghanistan',93), 
Array('Albania',355), 
Array('Algeria',213), 
Array('American Samoa',684), 
Array('Andorra',376), 
Array('Angola',244), 
Array('Anguilla',809), 
Array('Antarctica',672), 
Array('Antigua and Barbuda',1268), 
Array('Argentina',54), 
Array('Armenia',374), 
Array('Aruba',297), 
Array('Ascension',247), 
Array('Australia',61), 
Array('Austria',43), 
Array('Azerbaijan',994), 
Array('Bahamas',1242), 
Array('Bahrain',973), 
Array('Bangladesh',880), 
Array('Barbados',1246), 
Array('Belarus',375), 
Array('Belgium',32), 
Array('Belize',501), 
Array('Benin',229), 
Array('Bermuda',1441), 
Array('Bhutan',975), 
Array('Bolivia',591), 
Array('Bosnia and Herzegovina',387), 
Array('Botswana',267), 
Array('Brazil',55), 
Array('British Virgin Islands',284), 
Array('Brunei',673), 
Array('Bulgaria',359), 
Array('Burkina Faso',226), 
Array('Burundi',257), 
Array('Cambodia',855), 
Array('Cameroon',237), 
Array('Canada',1), 
Array('Cape Verde Islands',238), 
Array('Cayman Islands',345), 
Array('Central African Republic',236), 
Array('Chad',235), 
Array('Chatham Island (New Zealand)',64), 
Array('Chile',56), 
Array('China',86), 
Array('Christmas Island',672), 
Array('Cocos Islands',672), 
Array('Colombia',57), 
Array('Comoros',269), 
Array('Congo',242), 
Array('Cook Islands',682), 
Array('Costa Rica',506), 
Array('Croatia',385), 
Array('Cuba',53), 
Array('Cyprus',357), 
Array('Czech Republic',420), 
Array('Denmark',45), 
Array('Diego Garcia',246), 
Array('Djibouti',253), 
Array('Dominica',767), 
Array('Dominican Republic',809), 
Array('Easter Island',56), 
Array('Ecuador',593), 
Array('Egypt',20), 
Array('El Salvador',503), 
Array('Equatorial Guinea',240), 
Array('Eritrea',291), 
Array('Estonia',372), 
Array('Ethiopia',251), 
Array('Falkland Islands',500), 
Array('Faroe Islands',298), 
Array('Fiji',679), 
Array('Finland',358), 
Array('France',33), 
Array('French Antilles',596), 
Array('French Guyana',594), 
Array('French Polynesia',689), 
Array('Fyrom (Macedonia)',389), 
Array('Gabon',241), 
Array('Gambia',220), 
Array('Georgia',995), 
Array('Germany',49), 
Array('Ghana',233), 
Array('Gibraltar',350), 
Array('Greece',30), 
Array('Greenland',299), 
Array('Grenada and Carriacuou',473), 
Array('Grenadin Islands',784), 
Array('Guadeloupe',590), 
Array('Guam',671), 
Array('Guantanamo Bay',53), 
Array('Guatemala',502), 
Array('Guiana',594), 
Array('Guinea',224), 
Array('Guinea-Bissau',245), 
Array('Guyana',592), 
Array('Haiti',509), 
Array('Honduras',504), 
Array('Hong Kong',852), 
Array('Hungary',36), 
Array('Iceland',354), 
Array('India',91), 
Array('Indonesia',62), 
Array('Iran',98), 
Array('Iraq',964), 
Array('Ireland',353), 
Array('Israel',972), 
Array('Italy',39), 
Array('Ivory Coast',225), 
Array('Jamaica',876), 
Array('Japan',81), 
Array('Jerusalem',2), 
Array('Jordan',962), 
Array('Kazakhstan',7), 
Array('Kenya',254), 
Array('Kiribati',686), 
Array('South Korea',82), 
Array('Kuwait',965), 
Array('Kygyzstan',996), 
Array('Laos',856), 
Array('Latvia',371), 
Array('Lebanon',961), 
Array('Lesotho',266), 
Array('Liberia',231), 
Array('Libya',218), 
Array('Liechtenstein',41), 
Array('Lithuania',370), 
Array('Luxembourg',352), 
Array('Macau',853), 
Array('Macedonia',389), 
Array('Madagascar',261), 
Array('Malawi',265), 
Array('Malaysia',60), 
Array('Maldives',960), 
Array('Mali',223), 
Array('Malta',356), 
Array('Mariana Islands',670), 
Array('Marshall Islands',692), 
Array('Martinique',596), 
Array('Mauritania',222), 
Array('Mauritius',230), 
Array('Mayotte',269), 
Array('Mexico',52), 
Array('Micronesia,  Federated States',691), 
Array('Midway Islands',808), 
Array('Miquelon',508), 
Array('Moldova',373), 
Array('Monaco',377), 
Array('Mongolia',976), 
Array('Montserrat',664), 
Array('Morocco',212), 
Array('Mozambique',258), 
Array('Myanmar',95), 
Array('Namibia',264), 
Array('Nauru',674), 
Array('Nepal',977), 
Array('Netherlands',31), 
Array('Neth. Antilles',599), 
Array('Nevis',869), 
Array('New Caledonia',687), 
Array('New Zealand',64), 
Array('Nicaragua',505), 
Array('Niger',227), 
Array('Nigeria',234), 
Array('Niue',683), 
Array('Norfolk Island',672), 
Array('North Korea',850), 
Array('Norway',47), 
Array('Oman',968), 
Array('Pakistan',92), 
Array('Palau',680), 
Array('Panama',507), 
Array('Papua New Guinea',675), 
Array('Paraguay',595), 
Array('Peru',51), 
Array('Philippines',63), 
Array('Poland',48), 
Array('Portugal',351), 
Array('Principe',239), 
Array('Puerto Rico',1787), 
Array('Qatar',974), 
Array('Reunion Island',262), 
Array('Romania',40), 
Array('Russia',7), 
Array('Rwanda',250), 
Array('St. Helena',290), 
Array('St. Kitts',869), 
Array('St. Lucia',758), 
Array('St Pierre et Miquelon',508), 
Array('St. Vincent',784), 
Array('Saipan',670), 
Array('San Marino',378), 
Array('Sao Tome',239), 
Array('Saudi Arabia',966), 
Array('Senegal Republic',221), 
Array('Serbia',381), 
Array('Seychelles',248), 
Array('Sierra Leone',232), 
Array('Singapore',65), 
Array('Slovakia',421), 
Array('Slovenia',386), 
Array('Solomon Islands',677), 
Array('Somalia',252), 
Array('South Africa',27), 
Array('Spain',34), 
Array('Sri Lanka',94), 
Array('Sudan',249), 
Array('Suriname',597), 
Array('Swaziland',268), 
Array('Sweden',46), 
Array('Switzerland',41), 
Array('Syria',963), 
Array('Taiwan',886), 
Array('Tajikistan',7), 
Array('Tanzania',255), 
Array('Thailand',66), 
Array('Togo',228), 
Array('Tokelau',690), 
Array('Tonga',676), 
Array('Trinidad and Tobago',868), 
Array('Tunisia',216), 
Array('Turkey',90), 
Array('Turkmenistan',993), 
Array('Turks and Caicos Islands',649), 
Array('Tuvalu',688), 
Array('Uganda',256), 
Array('Ukraine',380), 
Array('United Arab Emirates',971), 
Array('United Kingdom',44), 
Array('United States',1), 
Array('Uruguay',598), 
Array('U.S. Virgin Islands',340), 
Array('Uzbekistan',7), 
Array('Vanuatu',678), 
Array('Vatican city',39), 
Array('Venezuela',58), 
Array('Vietnam',84), 
Array('Wake Island',808), 
Array('Wallis & Futuna Islands',681), 
Array('Western Samoa',685), 
Array('Yemen',967), 
Array('Yugoslavia',381), 
Array('Zaire',243), 
Array('Zambia',260), 
Array('Zanzibar',259), 
Array('Zimbabwe',263)
);

///////////Function for country
function ShowCountryOption()
{
	for(var x=0;x<ct_arr.length;x++)
	{
		document.write('<option value="' + ct_arr[x][0] + '">' + ct_arr[x][0] + '</option>\n');
	}
}

//////////////////////Function for ISD code
function getISDCode(sCtry)
{
	for(var x=0;x<ct_arr.length;x++)
	{
		if (sCtry == ct_arr[x][0])
		{
			
			document.queryform.code.value = '+'+ct_arr[x][1];
			//document.trainform.mobile.value = '+'+ct_arr[x][1]+'-';
		}
	}
}

function getISDCode3(sCtry)
{
	for(var x=0;x<ct_arr.length;x++)
	{
		if (sCtry == ct_arr[x][0])
		{
			
			document.quickcontactform.code.value = '+'+ct_arr[x][1];
			//document.trainform.mobile.value = '+'+ct_arr[x][1]+'-';
		}
	}
}


